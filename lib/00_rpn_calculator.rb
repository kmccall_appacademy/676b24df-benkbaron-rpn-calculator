class RPNCalculator

  def initialize
    @calculator = []
  end

  attr_reader :calculator

  def push (num)
    calculator << num
  end

  def execute(operation)
    raise "calculator is empty" if calculator.length < 2
    x = calculator.pop
    y = calculator.pop
    calculator << y.to_f.send(operation, x)
  end

  def plus
    execute(:+)
  end

  def minus
    execute(:-)
  end

  def times
    execute(:*)
  end

  def divide
    execute(:/)
  end

  def value
    calculator[-1]
  end

  def evaluate (str)
    elements = tokens(str)
    elements.each { |el| el.class == Fixnum ? calculator << el : execute(el) }

    value
  end

  def tokens (str)
    str.split.map do |el|
      if el.length > 1 || ("0".."9").include?(el)
         el.to_i
      else
        el.to_sym
      end
    end
  end

end
